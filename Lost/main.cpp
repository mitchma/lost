#include <iostream>
#include <chrono>
#include "header/Grid.hpp"
#include "header/AStar.hpp"
#include "header/JPS.hpp"

// TODO: refactor
AStar astar;
JPS jps;

/*
* Handle user input.
*
* @param window: SFML graphical window.
* @param gui: TGUI GUI.
* @param grid: Grid.
* @param dragCell: type of cell currently being dragged by mouse.
*/
void handleInput(sf::RenderWindow& window, tgui::Gui& gui, Grid& grid, Grid::CellType& dragCell)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		// close window
		if (event.type == sf::Event::Closed) { window.close(); }
		// resize
		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect visibleArea(0, 0, static_cast<float> (event.size.width), static_cast<float> (event.size.height));
			window.setView(sf::View(visibleArea));
			gui.setView(sf::View(visibleArea));
			gui.get("main")->setPosition((window.getSize().x - 10 - 130), 10);
			dragCell = Grid::CellType::empty;
			grid.reset();
		}
		// scroll
		if (event.type == sf::Event::MouseWheelScrolled && event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
		{
			grid.changeResolution(static_cast<int> (-event.mouseWheelScroll.delta));
			dragCell = Grid::CellType::empty;
			grid.reset();
		}
		// keyboard WASD movement and search
		if (event.type == sf::Event::KeyPressed)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) { window.close(); }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				grid.shiftCells(1);
				dragCell = Grid::CellType::empty;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				grid.shiftCells(0);
				dragCell = Grid::CellType::empty;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				grid.shiftCells(3);
				dragCell = Grid::CellType::empty;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				grid.shiftCells(2);
				dragCell = Grid::CellType::empty;
			}
			// find path
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				// algorithm
				unsigned algorithm = 0;
				if (gui.get<tgui::RadioButton>("jps", true)->isChecked()) { algorithm = 1; }

				// heuristic
				unsigned heuristic = 0;
				if (gui.get<tgui::RadioButton>("manhattan", true)->isChecked()) { heuristic = 1; }
				else if (gui.get<tgui::RadioButton>("euclidean", true)->isChecked()) { heuristic = 2; }
				else if (gui.get<tgui::RadioButton>("chebyshev", true)->isChecked()) { heuristic = 3; }

				// options
				bool shouldAnimate = gui.get<tgui::CheckBox>("animate", true)->isChecked();
				bool cutCorner = gui.get<tgui::CheckBox>("cutcorner", true)->isChecked();
				
				// search
				grid.clearCells();
				Grid::CellList path;
				
				auto start_time = std::chrono::high_resolution_clock::now();
				switch (algorithm)
				{
					case 0:
						path = astar.search(grid, heuristic, shouldAnimate, cutCorner);
						break;
					case 1:
						path = jps.search(grid, heuristic, shouldAnimate, cutCorner);
						break;
					default:
						path = astar.search(grid, heuristic, shouldAnimate, cutCorner);
				}
				auto end_time = std::chrono::high_resolution_clock::now();
				auto time = end_time - start_time;
				printf("Search took %fms.\n", static_cast<float> (std::chrono::duration_cast<std::chrono::microseconds>(time).count()) / 1000);

				grid.setPath(path);
				grid.render(true, true, true);
			}
		}
		// set draggable cells
		if (event.type == sf::Event::MouseButtonReleased)
		{
			sf::Vector2i windowCoordinate = sf::Mouse::getPosition(window);

			// get and fix grid coordinate
			sf::Vector2i gridCoordinate = grid.windowToGrid(windowCoordinate);
			if (!grid.isValidCoordinate(gridCoordinate, true)) { grid.fixGridCoordinate(gridCoordinate); }

			// get cell type at position to be replaced
			Grid::CellType cellType = grid.getCellType(gridCoordinate, true);

			// place down start/terminal cell
			if (dragCell == Grid::CellType::start)
			{
				grid.addStartCell(sf::Vector2i(gridCoordinate.x, gridCoordinate.y), true);
				dragCell = cellType == Grid::CellType::terminal ? Grid::CellType::terminal : Grid::CellType::empty;
			}
			else if (dragCell == Grid::CellType::terminal)
			{
				grid.addTerminalCell(sf::Vector2i(gridCoordinate.x, gridCoordinate.y), true);
				dragCell = cellType == Grid::CellType::start ? Grid::CellType::start : Grid::CellType::empty;
			}

			// fix flicker
			grid.renderCellAt(gridCoordinate.x, gridCoordinate.y);
			grid.renderLines();
		}
		// GUI input
		gui.handleEvent(event);
	}
	
	// drag start/terminal cells
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		// get position relative to window
		sf::Vector2i windowCoordinate = sf::Mouse::getPosition(window);
		sf::Vector2i gridCoordinate = grid.windowToGrid(windowCoordinate);
		Grid::CellType cellType = grid.getCellType(gridCoordinate, true);

		// set start or terminal cells to drag mode
		if (dragCell == Grid::CellType::empty && (cellType == Grid::CellType::start || cellType == Grid::CellType::terminal))
		{
			grid.removeCell(gridCoordinate, true);
			dragCell = cellType;
		}
	}
	// remove cells
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		// get position relative to window
		sf::Vector2i windowCoordinate = sf::Mouse::getPosition(window);
		sf::Vector2i gridCoordinate = grid.windowToGrid(windowCoordinate);

		Grid::CellType cellType = grid.getCellType(gridCoordinate, true);

		if (cellType != Grid::CellType::start && cellType != Grid::CellType::terminal)
		{
			grid.removeCell(gridCoordinate, true);
		}
	}
	// add walls
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
	{
		// get position relative to window
		sf::Vector2i windowCoordinate = sf::Mouse::getPosition(window);
		sf::Vector2i gridCoordinate = grid.windowToGrid(windowCoordinate);
		Grid::CellType cellType = grid.getCellType(gridCoordinate, true);

		//if (dragCell != Grid::CellType::start && dragCell != Grid::CellType::terminal)
		if (cellType != Grid::CellType::start && cellType != Grid::CellType::terminal)
		{
			grid.addWallCell(gridCoordinate, true);
		}
	}
	// render dragged cell
	if (dragCell != Grid::CellType::empty)
	{
		sf::Vector2i windowCoordinate = sf::Mouse::getPosition(window);

		// get and fix grid coordinate
		sf::Vector2i gridCoordinate = grid.windowToGrid(windowCoordinate);
		if (!grid.isValidCoordinate(gridCoordinate, true)) { grid.fixGridCoordinate(gridCoordinate); }

		grid.renderCell(gridCoordinate, grid.cellTypeToColor(dragCell), true);
	}
}

/*
* Add UI elements.
*
* @param window: SFML graphical window.
* @param gui: TGUI GUI.
*/
void setGUIElements(sf::RenderWindow& window, tgui::Gui& gui)
{
	const int LEFT = static_cast<int> (window.getSize().x * 0.8f);
	const int TEXT_SIZE = 18;
	const int WIDTH = 130;
	const float OPACITY = 0.6f;

	// main panel
	tgui::Panel::Ptr main = tgui::Panel::create({WIDTH, 340});
	gui.add(main, "main");
	main->setPosition(LEFT, 10);
	main->setOpacity(OPACITY);

	// search method
	tgui::Panel::Ptr searches = tgui::Panel::create({WIDTH, 90});
	main->add(searches, "searches");
	searches->setBackgroundColor(tgui::Color(0, 0, 0, 0));
	searches->setPosition(0, 0);

	tgui::Label::Ptr searchLabel = tgui::Label::create("Algorithm");
	searches->add(searchLabel, "label");
	searchLabel->setTextStyle(sf::Text::Bold);
	searchLabel->setTextSize(TEXT_SIZE);

	tgui::RadioButton::Ptr astar = tgui::RadioButton::create();
	searches->add(astar, "astar");
	astar->setSize({TEXT_SIZE, TEXT_SIZE});
	astar->setTextSize(TEXT_SIZE);
	astar->setText("A*");
	astar->setPosition(0, 30);
	astar->check();
	
	tgui::RadioButton::Ptr jps = tgui::RadioButton::create();
	searches->add(jps, "jps");
	jps->setSize({TEXT_SIZE, TEXT_SIZE});
	jps->setTextSize(TEXT_SIZE);
	jps->setText("JPS");
	jps->setPosition(0, 60);

	// heuristic
	tgui::Panel::Ptr heuristics = tgui::Panel::create({WIDTH, 150});
	main->add(heuristics);
	heuristics->setBackgroundColor(tgui::Color(0, 0, 0, 0));
	heuristics->setPosition(0, 100);

	tgui::Label::Ptr heuristicLabel = tgui::Label::create("Heuristic");
	heuristics->add(heuristicLabel, "label");
	heuristicLabel->setTextStyle(sf::Text::Bold);
	heuristicLabel->setTextSize(TEXT_SIZE);

	tgui::RadioButton::Ptr octile = tgui::RadioButton::create();
	heuristics->add(octile, "octile");
	octile->setSize({TEXT_SIZE, TEXT_SIZE});
	octile->setTextSize(TEXT_SIZE);
	octile->setText("Octile");
	octile->setPosition(0, 30);
	octile->check();
	tgui::RadioButton::Ptr manhattan = tgui::RadioButton::create();
	heuristics->add(manhattan, "manhattan");
	manhattan->setSize({TEXT_SIZE, TEXT_SIZE});
	manhattan->setTextSize(TEXT_SIZE);
	manhattan->setText("Manhattan");
	manhattan->setPosition(0, 60);
	tgui::RadioButton::Ptr euclidean = tgui::RadioButton::create();
	heuristics->add(euclidean, "euclidean");
	euclidean->setSize({TEXT_SIZE, TEXT_SIZE});
	euclidean->setTextSize(TEXT_SIZE);
	euclidean->setText("Euclidean");
	euclidean->setPosition(0, 90);
	tgui::RadioButton::Ptr chebyshev = tgui::RadioButton::create();
	heuristics->add(chebyshev, "chebyshev");
	chebyshev->setSize({TEXT_SIZE, TEXT_SIZE});
	chebyshev->setTextSize(TEXT_SIZE);
	chebyshev->setText("Chebyshev");
	chebyshev->setPosition(0, 120);

	// additional options
	tgui::Panel::Ptr options = tgui::Panel::create({WIDTH, 90});
	main->add(options);
	options->setBackgroundColor(tgui::Color(0, 0, 0, 0));
	options->setPosition(0, 260);

	tgui::Label::Ptr optionLabel = tgui::Label::create("Options");
	options->add(optionLabel, "label");
	optionLabel->setTextStyle(sf::Text::Bold);
	optionLabel->setTextSize(TEXT_SIZE);

	tgui::CheckBox::Ptr animate = tgui::CheckBox::create();
	options->add(animate, "animate");
	animate->setSize({TEXT_SIZE, TEXT_SIZE});
	animate->setTextSize(TEXT_SIZE);
	animate->setPosition(0, 30);
	animate->setText("Animate");

	tgui::CheckBox::Ptr cutCorner = tgui::CheckBox::create();
	options->add(cutCorner, "cutcorner");
	cutCorner->setSize({TEXT_SIZE, TEXT_SIZE});
	cutCorner->setTextSize(TEXT_SIZE);
	cutCorner->setPosition(0, 60);
	cutCorner->setText("Cut corner");
}

/*
* Main.
*/
int main()
{
	// create window
	sf::RenderWindow window(sf::VideoMode(800, 600), "Lost");
	window.setFramerateLimit(60);

	// create GUI object
	tgui::Gui gui(window);
	setGUIElements(window, gui);
	
	// create grid
	Grid grid(window);
	Grid::CellType dragCell = Grid::CellType::empty;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		grid.render(false);
		handleInput(window, gui, grid, dragCell);
		grid.renderLines();
		gui.draw();
		window.display();
	}

	return 0;
}
