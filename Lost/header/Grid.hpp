#pragma once
#ifndef GRID_H
#define GRID_H
#include <TGUI/TGUI.hpp>

class Grid
{
	public:
		typedef std::vector<sf::Vector2i> CellList;
		typedef std::vector<CellList> CellMatrix;
		enum CellType { empty, expanded, openList, route, start, terminal, wall, unknown };

		std::vector<std::vector<CellType>> cells;
		CellList path;
		int resolution;
		sf::Vector2i startCoordinate;
		sf::Vector2i terminalCoordinate;
		sf::RenderWindow& window;

		// initialization
		Grid(sf::RenderWindow& window);

		// cell edit
		void addCell(sf::Vector2i coordinate, CellType type, bool isGridCoordinate = false);
		void addEmptyCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addExpandedCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addOpenListCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addRouteCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addStartCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addTerminalCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void addWallCell(sf::Vector2i coordinate, bool isGridCoordinate = false);
		void removeCell(sf::Vector2i coordinate, bool isGridCoordinate = false);

		// manipulation
		void changeResolution(int amount = 0);
		void fixGridCoordinate(sf::Vector2i& gridCoordinate);
		void shiftCells(int direction);
		void verifyStartTerminal();
		sf::Vector2i windowToGrid(sf::Vector2i windowCoordinate);

		// render
		void clear();
		void clearCells();
		void render(bool shouldRenderLines = true, bool shouldDisplay = false, bool shouldRenderPath = true);
		void renderCell(sf::Vector2i coordinate, sf::Color color, bool isGridCoordinate = false);
		void renderCellAt(int x, int y);
		void renderCells();
		void renderLines();
		void renderPath();

		// getter and checker
		sf::Color cellTypeToColor(CellType type);
		std::vector<std::vector<CellType>> getCells();
		CellType getCellType(sf::Vector2i coordinate, bool isGridCoordinate = false);
		sf::Vector2i getSize();
		sf::Vector2i getStartCoordinate();
		sf::Vector2i getTerminalCoordinate();
		bool isTraversable(sf::Vector2i cell);
		bool isValidCoordinate(sf::Vector2i coordinate, bool isGridCoordinate = false);

		// setter
		void reset();
		void setPath(CellList path);
};

#endif
