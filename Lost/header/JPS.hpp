#pragma once
#ifndef JPS_H
#define JPS_H
#include "AStar.hpp"

class JPS : public AStar
{
	private:
		static Grid::CellList getSuccessors(Grid& grid, sf::Vector2i cell, sf::Vector2i direction);
		static Grid::CellList getForcedNeighbors(Grid& grid, sf::Vector2i cell, sf::Vector2i direction);
		static sf::Vector2i jump(Grid& grid, sf::Vector2i cell, sf::Vector2i direction);
	protected:
		Grid::CellList getPath(Grid& grid, std::vector<Grid::CellList> cameFrom, bool cutCorner) override;
		Grid::CellList getPotentialCells(Grid& grid, sf::Vector2i cell, sf::Vector2i parent, bool cutCorner = false) override;
};

#endif
