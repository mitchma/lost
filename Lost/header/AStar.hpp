#pragma once
#ifndef ASTAR_H
#define ASTAR_H
#include <vector>
#include <SFML/graphics.hpp>
#include "Grid.hpp"

class AStar
{
	public:
		Grid::CellList search(Grid& grid, unsigned heuristic = 0, bool shouldAnimate = false, bool cutCorner = false);
	private:
		static Grid::CellList getNeighbors(Grid& grid, sf::Vector2i cell, bool cutCorner = false);
	protected:
		static float getHeuristic(sf::Vector2i start, sf::Vector2i target, unsigned heuristic = 0);
		virtual Grid::CellList getPath(Grid& grid, std::vector<Grid::CellList> cameFrom, bool cutCorner);
		virtual Grid::CellList getPotentialCells(Grid& grid, sf::Vector2i cell, sf::Vector2i parent, bool cutCorner = false);
};

#endif
