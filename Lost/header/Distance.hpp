#pragma once
#ifndef DISTANCE_H
#define DISTANCE_H
#include <SFML/graphics.hpp>

namespace Distance
{
	float chebyshevDistance(sf::Vector2i coord1, sf::Vector2i coord2);
	float euclideanDistance(sf::Vector2i coord1, sf::Vector2i coord2);
	float manhattanDistance(sf::Vector2i coord1, sf::Vector2i coord2);
	float octileDistance(sf::Vector2i coord1, sf::Vector2i coord2);
	sf::Vector2i unitize(sf::Vector2i vec);
}

#endif
