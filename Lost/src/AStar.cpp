#include "../header/AStar.hpp"
#include "../header/Distance.hpp"

/**********************************************************************************************************************
**                                                      Search                                                       **
**********************************************************************************************************************/

/*
* Search the grid for a path.
*
* @param shouldAnimate: If true, animate changes to open list. Default is false.
* @return: Path from terminal to starting cell. Empty path returned upon failure.
*/
Grid::CellList AStar::search(Grid& grid, unsigned heuristic, bool shouldAnimate, bool cutCorner)
{
	const float INFTY = std::numeric_limits<float>::infinity();
	const sf::Vector2i NULL_VECTOR = sf::Vector2i(-1, -1);

	// fScore and gScore defaulted to INF for all cells
	sf::Vector2i cellDimension = grid.getSize();
	std::vector<std::vector<float>> fScore(cellDimension.x, std::vector<float>(cellDimension.y, INFTY));
	std::vector<std::vector<float>> gScore(cellDimension.x, std::vector<float>(cellDimension.y, INFTY));
	Grid::CellMatrix cameFrom(cellDimension.x, Grid::CellList(cellDimension.y, NULL_VECTOR));

	// closed set defaulted to false
	std::vector<std::vector<bool>> closedSet(cellDimension.x, std::vector<bool>(cellDimension.y, false));

	// queue of cells that should be evaluated (ordered by f-score)
	Grid::CellList pq;

	// set gScore and fScore for starting cell
	sf::Vector2i start = grid.getStartCoordinate();
	sf::Vector2i terminal = grid.getTerminalCoordinate();
	gScore[start.x][start.y] = 0;
	fScore[start.x][start.y] = getHeuristic(start, terminal, heuristic);

	// initially, only start coordinate should be evaluated
	pq.push_back(start);

	// exhaust all possible cells
	while (!pq.empty())
	{
		// debugging: verify set order
		if (false)
		{
			Grid::CellList pqq = pq;
			Grid::CellList vec2check;
			while (!pqq.empty())
			{
				sf::Vector2i curr = pqq.back();
				//printf("(%i,%i): %f\n", curr.x, curr.y, fScore[curr.x][curr.y]);
				vec2check.push_back(curr);
				pqq.pop_back();
			}
			for (size_t i = 0; i < vec2check.size() - 1; i++)
			{
				if (fScore[vec2check[i].x][vec2check[i].y] > fScore[vec2check[i + 1].x][vec2check[i + 1].y])
				{
					printf("wrong\n");
				}
			}
		}

		// get lowest f-score cell
		sf::Vector2i curr = pq.back();

		// goal is found
		if (curr == terminal) { return getPath(grid, cameFrom, cutCorner); }

		// update closed set
		closedSet[curr.x][curr.y] = true;
		if (curr != start && curr != terminal) { grid.addExpandedCell(curr, true); }
		if (shouldAnimate) { grid.render(true, true, false); }
		pq.pop_back();

		// add neighbors
		for (sf::Vector2i cell : getPotentialCells(grid, curr, cameFrom[curr.x][curr.y], cutCorner))
		{
			// node has already been evaluated
			if (closedSet[cell.x][cell.y] == true) { continue; }

			// distance between current and neighbor cells
			float cost = Distance::euclideanDistance(curr, cell);
			float potentialGScore = gScore[curr.x][curr.y] + cost;

			// erase duplicate
			for (Grid::CellList::iterator it = pq.begin(); it != pq.end(); it++)
			{
				if (*it == cell)
				{
					pq.erase(it);
					break;
				}
			}

			// new path is shorter
			if (potentialGScore < gScore[cell.x][cell.y])
			{
				cameFrom[cell.x][cell.y] = curr;
				gScore[cell.x][cell.y] = potentialGScore;
				fScore[cell.x][cell.y] = potentialGScore + getHeuristic(cell, terminal, heuristic);
			}

			if (cell != start && cell != terminal) { grid.addOpenListCell(cell, true); }
			if (shouldAnimate) { grid.render(true, true, false); }
			pq.push_back(cell);
		}

		// sort vector based on f-score
		auto comp = [&](sf::Vector2i v1, sf::Vector2i v2) { return fScore[v1.x][v1.y] > fScore[v2.x][v2.y]; };
		std::sort(pq.begin(), pq.end(), comp);
	}

	Grid::CellList nothing;
	return nothing;
}

/**********************************************************************************************************************
**                                                      Getter                                                       **
**********************************************************************************************************************/

/*
* Get heuristic between two cell coordinates.
*
* @param from: 2D coordinate.
* @param to: 2D coordinate.
* @param heuristic: Specific type of heuristic. Default is 0 (octile distance) which is best for 8-neighbour grids.
*                   0: Octile
*                   1: Manhattan (L1)
*                   2: Euclidean (L2)
*                   3: Chebyshev (L-inf)
* @return: Heuristic value.
*/
float AStar::getHeuristic(sf::Vector2i from, sf::Vector2i to, unsigned heuristic)
{
	switch (heuristic)
	{
		case 0:
			return Distance::octileDistance(from, to);
		case 1:
			return Distance::manhattanDistance(from, to);
		case 2:
			return Distance::euclideanDistance(from, to);
		case 3:
			return Distance::chebyshevDistance(from, to);
		default:
			return Distance::octileDistance(from, to);
	}
}

/*
* Construct path.
*
* @param grid: Grid instance.
* @param cameFrom: 2D vector recording the origin of each cell.
* @param cutCorner: If true, no diagonal movement allowed beside corner.
* @return: List of coordinates (from the terminal to starting cell) representing the path to take.
*/
Grid::CellList AStar::getPath(Grid& grid, std::vector<Grid::CellList> cameFrom, bool cutCorner)
{
	Grid::CellList path = {grid.getTerminalCoordinate()};
	sf::Vector2i curr = grid.getTerminalCoordinate();
	const sf::Vector2i NULL_VECTOR = sf::Vector2i(-1, -1);

	// while current cell still has originator
	while (cameFrom[curr.x][curr.y] != NULL_VECTOR)
	{
		// push parent onto path
		path.push_back(cameFrom[curr.x][curr.y]);
		curr = cameFrom[curr.x][curr.y];
	}

	return path;
}

/*
* Get neighbours of given cell. Generic A* neighbours.
*
* @param cell: 2D grid coordinate.
* @param cutCorner: If true, allows diagonal movement against wall. Default is false.
* @return: List of potential neighbors of the given cell.
*/
Grid::CellList AStar::getNeighbors(Grid& grid, sf::Vector2i cell, bool cutCorner)
{
	const sf::Vector2i NORTH = sf::Vector2i(0, -1);
	const sf::Vector2i EAST = sf::Vector2i(1, 0);
	const sf::Vector2i SOUTH = sf::Vector2i(0, 1);
	const sf::Vector2i WEST = sf::Vector2i(-1, 0);
	const sf::Vector2i NORTHEAST = sf::Vector2i(1, -1);
	const sf::Vector2i NORTHWEST = sf::Vector2i(-1, -1);
	const sf::Vector2i SOUTHEAST = sf::Vector2i(1, 1);
	const sf::Vector2i SOUTHWEST = sf::Vector2i(-1, 1);

	sf::Vector2i directions[] = {
		cell + NORTH,
		cell + EAST,
		cell + SOUTH,
		cell + WEST,
		cell + NORTHEAST,
		cell + NORTHWEST,
		cell + SOUTHEAST,
		cell + SOUTHWEST,
	};
	bool potentialNeighbors[] = {
		grid.isTraversable(directions[0]),
		grid.isTraversable(directions[1]),
		grid.isTraversable(directions[2]),
		grid.isTraversable(directions[3]),
		grid.isTraversable(directions[4]),
		grid.isTraversable(directions[5]),
		grid.isTraversable(directions[6]),
		grid.isTraversable(directions[7]),
	};
	Grid::CellList neighbors;

	// remove inaccessible cells
	// north and east blocked => northeast inaccessible
	if (!potentialNeighbors[0] && !potentialNeighbors[1]) { potentialNeighbors[4] = false; }
	// north and west blocked => northwest inaccessible
	if (!potentialNeighbors[0] && !potentialNeighbors[3]) { potentialNeighbors[5] = false; }
	// south and east blocked => southeast inaccessible
	if (!potentialNeighbors[2] && !potentialNeighbors[1]) { potentialNeighbors[6] = false; }
	// south and west blocked => southwest inaccessible
	if (!potentialNeighbors[2] && !potentialNeighbors[3]) { potentialNeighbors[7] = false; }

	// must walk around corner
	if (!cutCorner)
	{
		// north is obstructed
		if (!potentialNeighbors[0])
		{
			potentialNeighbors[0] = false;
			potentialNeighbors[4] = false;
			potentialNeighbors[5] = false;
		}
		// east is obstructed
		if (!potentialNeighbors[1])
		{
			potentialNeighbors[1] = false;
			potentialNeighbors[4] = false;
			potentialNeighbors[6] = false;
		}
		// south is obstructed
		if (!potentialNeighbors[2])
		{
			potentialNeighbors[2] = false;
			potentialNeighbors[6] = false;
			potentialNeighbors[7] = false;
		}
		// west is obstructed
		if (!potentialNeighbors[3])
		{
			potentialNeighbors[3] = false;
			potentialNeighbors[5] = false;
			potentialNeighbors[7] = false;
		}
	}

	// add potential neighbors
	for (int dir_key = 0; dir_key < 8; dir_key++)
	{
		if (potentialNeighbors[dir_key]) { neighbors.push_back(directions[dir_key]); }
	}

	return neighbors;
}

/*
* Get potential cells to add to open list from given cell. This is mainly for inheritance.
*
* @param cell: 2D grid coordinate.
* @param parent: 2D grid coordinate from which the given cell came.
* @param cutCorner: If true, allows diagonal movement against wall. Default is false.
* @return: List of potential open list cells.
*/
Grid::CellList AStar::getPotentialCells(Grid& grid, sf::Vector2i cell, sf::Vector2i parent, bool cutCorner)
{
	return getNeighbors(grid, cell, cutCorner);
}
