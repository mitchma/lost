#include "../header/Grid.hpp"

/**********************************************************************************************************************
**                                                  Initialization                                                   **
**********************************************************************************************************************/

/*
* Constructor.
*
* @param window: Graphical SFML window.
*/
Grid::Grid(sf::RenderWindow& window) : window(window) {
	resolution = 20;
	Grid::reset();
}

/**********************************************************************************************************************
**                                                    Cell Edit                                                      **
**********************************************************************************************************************/

/*
* Add cell to grid.
*
* @param coordinate: 2D coordinate.
* @param type: Cell type for the coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addCell(sf::Vector2i coordinate, CellType type = empty, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	if (!Grid::isValidCoordinate(coordinate, isGridCoordinate)) { return; }
	cells[coordinate.x][coordinate.y] = type;
}

/*
* Add empty cell to grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addEmptyCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	Grid::addCell(coordinate, empty, isGridCoordinate);
}

/*
* Add expanded (close list) cell to grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addExpandedCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	Grid::addCell(coordinate, expanded, isGridCoordinate);
}

/*
* Add open list cell to the grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addOpenListCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	Grid::addCell(coordinate, openList, isGridCoordinate);
}

/*
* Add route cell to grid. This is unused, as paths are rendered with lines.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addRouteCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	Grid::addCell(coordinate, route, isGridCoordinate);
}

/*
* Add start cell to grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addStartCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	Grid::addCell(coordinate, start, true);
	startCoordinate = coordinate;
}

/*
* Add terminal cell to grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addTerminalCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	Grid::addCell(coordinate, terminal, true);
	terminalCoordinate = coordinate;
}

/*
* Add wall cell to grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::addWallCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	Grid::addCell(coordinate, wall, isGridCoordinate);
}

/*
* Remove cell by making it an empty type.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::removeCell(sf::Vector2i coordinate, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	if (!Grid::isValidCoordinate(coordinate, isGridCoordinate)) { return; }
	cells[coordinate.x][coordinate.y] = empty;
}

/**********************************************************************************************************************
**                                                   Manipulation                                                    **
**********************************************************************************************************************/

/*
* Change resolution by constant. Higher resolution means more grids.
*
* @param amount: Delta.
*/
void Grid::changeResolution(int amount)
{
	resolution = resolution + amount > 1 ? resolution + amount : 1;
}

/*
* Bound provided coordinate into grid range.
*
* @param gridCoordinate: 2D grid coordinate.
*/
void Grid::fixGridCoordinate(sf::Vector2i& gridCoordinate)
{
	if (gridCoordinate.x < 0) { gridCoordinate.x = 0; }
	else if (gridCoordinate.x > static_cast<int> (cells.size()) - 1) { gridCoordinate.x = cells.size() - 1; }
	if (gridCoordinate.y < 0) { gridCoordinate.y = 0; }
	else if (gridCoordinate.y > static_cast<int> (cells[0].size()) - 1) { gridCoordinate.y = cells[0].size() - 1; }
}

/*
* Shift cells in given direction by 1 grid unit.
*
* @param direction: 0 is up, 1 is down, 2 is left, 3 is right
*/
void Grid::shiftCells(int direction)
{
	// shift up
	if (direction == 0)
	{
		for (size_t x = 0; x < cells.size(); x++)
		{
			for (size_t y = 0; y < cells[0].size(); y++)
			{
				cells[x][y] = y == cells[0].size() - 1 ? empty : cells[x][y + 1];
			}
		}
	}
	// shift down
	else if (direction == 1)
	{
		for (size_t x = 0; x < cells.size(); x++)
		{
			for (int y = static_cast<int> (cells[0].size()) - 1; y >= 0; y--)
			{
				cells[x][y] = y == 0 ? empty : cells[x][y - 1];
			}
		}
	}
	// shift left
	else if (direction == 2)
	{
		for (size_t x = 0; x < cells.size(); x++)
		{
			for (size_t y = 0; y < cells[0].size(); y++)
			{
				cells[x][y] = x == cells.size() - 1 ? empty : cells[x + 1][y];
			}
		}
	}
	// shift right
	else
	{
		for (int x = static_cast<int> (cells.size()) - 1; x >= 0; x--)
		{
			for (size_t y = 0; y < cells[0].size(); y++)
			{
				cells[x][y] = x == 0 ? empty : cells[x - 1][y];
			}
		}
	}

	Grid::verifyStartTerminal();
}

/*
* Check if start and terminal cells exist. Add start or terminal cells if missing.
*/
void Grid::verifyStartTerminal()
{
	// ensure start and terminal exist
	bool hasStart = false;
	bool hasTerminal = false;

	// ensure start exist
	for (size_t x = 0; x < cells.size(); x++)
	{
		for (size_t y = 0; y < cells[0].size(); y++)
		{
			if (cells[x][y] == start) { hasStart = true; }
			else if (cells[x][y] == terminal) { hasTerminal = true; }
		}
	}
	if (!hasStart) {
		if (cells[0][0] == terminal)
		{
			hasTerminal = true;
			Grid::addTerminalCell(sf::Vector2i(1, 0), true);
		}
		Grid::addStartCell(sf::Vector2i(0, 0), true);
	}
	if (!hasTerminal) {
		if (cells[1][0] == start)
		{
			Grid::addStartCell(sf::Vector2i(0, 0), true);
		}
		Grid::addTerminalCell(sf::Vector2i(1, 0), true);
	}
}

/*
* Convert window coordinate into grid coordinate.
*
* @param windowCoordinate: Pixel coordinate relative to window application.
*/
sf::Vector2i Grid::windowToGrid(sf::Vector2i windowCoordinate)
{
	// get X coordinate
	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (cells.size());
	int gridX = static_cast<int> (static_cast<float> (windowCoordinate.x) / dim_x);

	// get Y coordinate
	float dim_y = static_cast<float> (window.getSize().y) / static_cast<float> (cells[0].size());
	int gridY = static_cast<int> (static_cast<float> (windowCoordinate.y) / dim_y);

	return sf::Vector2i(gridX, gridY);
}

/**********************************************************************************************************************
**                                                      Render                                                       **
**********************************************************************************************************************/

/*
* Clear window.
*/
void Grid::clear()
{
	window.clear(sf::Color(250, 250, 250, 255));
}

/*
* Clear all cells except for start and terminal cells.
*/
void Grid::clearCells()
{
	for (size_t x = 0; x < cells.size(); x++)
	{
		for (size_t y = 0; y < cells[0].size(); y++)
		{
			if (cells[x][y] != start && cells[x][y] != terminal && cells[x][y] != wall) { cells[x][y] = empty; }
		}
	}
	render(true, true, true);
}

/*
* Convert window coordinate into grid coordinate.
*
* @param shouldRenderLines: If true, render grid lines.
* @param shouldDisplay: If true, display to window (window will visually update graphics).
* @param shouldRenderPath: If true, render path if one exists.
*/
void Grid::render(bool shouldRenderLines, bool shouldDisplay, bool shouldRenderPath)
{
	clear();
	renderCells();
	if (shouldRenderLines) { renderLines(); }
	if (shouldRenderPath && !path.empty()) { renderPath(); }
	if (shouldDisplay) { window.display(); }
}

/*
* Render cell on window. Note that cells is not modified so rerendering window will cause this render to disappear.
*
* @param coordinate: 2D coordinate.
* @param color: Color of cell.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
void Grid::renderCell(sf::Vector2i coordinate, sf::Color color, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	if (!Grid::isValidCoordinate(coordinate, true)) { return; }

	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (cells.size());
	float dim_y = static_cast<float> (window.getSize().y) / static_cast<float> (cells[0].size());

	sf::RectangleShape cell;
	cell.setSize(sf::Vector2f(dim_x, dim_y));
	cell.setFillColor(color);
	cell.setPosition(static_cast<float> (coordinate.x) * dim_x, static_cast<float> (coordinate.y) * dim_y);
	window.draw(cell);
}

/*
* Render cell at given index in grid.
*
* @param x: x-coordinate
* @param y: y-coordinate
*/
void Grid::renderCellAt(int x, int y)
{
	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (cells.size());
	float dim_y = static_cast<float> (window.getSize().y) / static_cast<float> (cells[0].size());

	// render non-empty cells
	if (cells[x][y] != empty)
	{
		sf::RectangleShape cell;
		cell.setSize(sf::Vector2f(dim_x, dim_y));
		cell.setFillColor(Grid::cellTypeToColor(cells[x][y]));
		cell.setPosition(static_cast<float> (x) * dim_x, static_cast<float> (y) * dim_y);
		window.draw(cell);
	}
}

/*
* Render all cells in grid.
*/
void Grid::renderCells()
{
	for (size_t x = 0; x < cells.size(); x++)
	{
		for (size_t y = 0; y < cells[0].size(); y++)
		{
			if (cells[x][y] != empty)
			{
				Grid::renderCellAt(x, y);
			}
		}
	}
}

/*
* Render vertical and horizontal grid lines.
*/
void Grid::renderLines()
{
	sf::Color lineColor = sf::Color(200, 200, 200, 255);

	// draw vertical lines
	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (cells.size());
	for (size_t x = 0; x < cells.size(); x++)
	{
		sf::Vertex line[] = {
			sf::Vertex(sf::Vector2f(static_cast<float> (x) * dim_x, 0.0f), lineColor),
			sf::Vertex(sf::Vector2f(static_cast<float> (x) * dim_x, static_cast<float> (window.getSize().y)), lineColor),
		};
		window.draw(line, 2, sf::Lines);
	}

	// draw horizontal lines
	float dim_y = static_cast<float> (window.getSize().y) / static_cast<float> (cells[0].size());
	for (size_t y = 0; y < cells[0].size(); y++)
	{
		sf::Vertex line[] = {
			sf::Vertex(sf::Vector2f(0.0f, static_cast<float> (y) * dim_y), lineColor),
			sf::Vertex(sf::Vector2f(static_cast<float> (window.getSize().x), static_cast<float> (y) * dim_y), lineColor),
		};
		window.draw(line, 2, sf::Lines);
	}
}

/*
* Render path if one exists.
*/
void Grid::renderPath()
{
	if (path.empty()) { return; }

	// draw path
	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (cells.size());
	float dim_y = static_cast<float> (window.getSize().y) / static_cast<float> (cells[0].size());

	const sf::Color pathColor = cellTypeToColor(CellType::route);

	sf::VertexArray lines(sf::LineStrip, path.size());
	for (size_t i = 0; i < path.size(); i++)
	{
		lines[i].position = sf::Vector2f(
			(static_cast<float> (path[i].x) + 0.5f) * dim_x,
			(static_cast<float> (path[i].y) + 0.5f) * dim_y
		);
		lines[i].color = pathColor;
	}

	window.draw(lines);
}

/**********************************************************************************************************************
**                                                Getter and Checker                                                 **
**********************************************************************************************************************/

/*
* Return color of given cell type.
*
* @param type: Cell type.
* @return: Color.
*/
sf::Color Grid::cellTypeToColor(CellType type)
{
	switch (type)
	{
	case expanded:
		return sf::Color(140, 140, 255, 255);
	case openList:
		return sf::Color(220, 220, 255, 255);
	case route:
		return sf::Color(140, 50, 230, 255);
	case start:
		return sf::Color(0, 255, 0, 255);
	case terminal:
		return sf::Color(255, 0, 0, 255);
	case wall:
		return sf::Color(80, 80, 80, 255);
	default:
		return sf::Color(0, 0, 0, 255);
	}
}

/*
* Get cells.
*
* @return: 2D vector of cell types.
*/
std::vector<std::vector<Grid::CellType>> Grid::getCells()
{
	return cells;
}

/*
* Return type of cell at coordinate. If cell does not exist, return unknown.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
* @return: Type of cell.
*/
Grid::CellType Grid::getCellType(sf::Vector2i coordinate, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	if (!isValidCoordinate(coordinate, true)) { return unknown; }
	return cells[coordinate.x][coordinate.y];
}

/*
* Return dimension of grid.
*
* @param coordinate: 2D coordinate.
* @return: Dimensional vector.
*/
sf::Vector2i Grid::getSize()
{
	return sf::Vector2i(cells.size(), cells[0].size());
}

/*
* Get start coordinate.
*
* @return: 2D coordinate.
*/
sf::Vector2i Grid::getStartCoordinate()
{
	return startCoordinate;
}

/*
* Get terminal coordinate.
*
* @return: 2D coordinate.
*/
sf::Vector2i Grid::getTerminalCoordinate()
{
	return terminalCoordinate;
}

/*
* Return true if given cell is traversable.
*
* @param coordinate: 2D coordinate.
* @return: True if cell is traversable, else false.
*/
bool Grid::isTraversable(sf::Vector2i cell)
{
	// cell must be within grid bounds
	bool cellExists = cell.x >= 0 && cell.x < static_cast<int> (cells.size()) && cell.y >= 0 && cell.y < static_cast<int> (cells[0].size());
	return cellExists && cells[cell.x][cell.y] != wall;
}

/*
* Return true if given coordinate is within range of grid.
*
* @param coordinate: 2D coordinate.
* @param isGridCoordinate: If true, provided coordinate serve as index to cells, else window coordinate.
*/
bool Grid::isValidCoordinate(sf::Vector2i coordinate, bool isGridCoordinate)
{
	if (!isGridCoordinate) { coordinate = Grid::windowToGrid(coordinate); }
	bool xInRange = coordinate.x >= 0 && coordinate.x < static_cast<int> (cells.size());
	bool yInRange = coordinate.y >= 0 && coordinate.y < static_cast<int> (cells[0].size());
	return xInRange && yInRange;
}

/**********************************************************************************************************************
**                                                      Setter                                                       **
**********************************************************************************************************************/

/*
* Reset grid size based on resolution and window size. Reset verifies if start and terminal cells exist and rerender.
*/
void Grid::reset()
{
	int x_size = resolution > 1 ? resolution : 2;
	cells.resize(x_size);

	float dim_x = static_cast<float> (window.getSize().x) / static_cast<float> (resolution);
	int y_size = static_cast<int> (static_cast<float> (window.getSize().y) / dim_x);
	if (y_size < 1) { y_size = 1; }

	for (int x = 0; x < x_size; x++)
	{
		cells[x].resize(y_size, empty);
		/*for (size_t y = 0; y < cells[0].size(); y++)
		{
			cells[x][y] = empty;
		}*/
	}

	Grid::verifyStartTerminal();
	Grid::render();
}

/*
* Set path.
*
* @param path: Path going from terminal to start cell.
*/
void Grid::setPath(CellList path)
{
	this->path = path;
}
