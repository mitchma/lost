#include "../header/JPS.hpp"
#include "../header/Distance.hpp"

/**********************************************************************************************************************
**                                                Jump Point Search                                                  **
**********************************************************************************************************************/

/*
* Return list of forced neighbours. Nontraversable forced neighbours will be filtered.
*
* @param grid: Grid instance.
* @param cell: 2D grid coordinate that serve as pivot of jump.
* @param direction: Direction from given cell and its parent. This is the propagation direction used for JPS' jump.
* @return: List of forced neighbours.
*/
Grid::CellList JPS::getForcedNeighbors(Grid& grid, sf::Vector2i cell, sf::Vector2i direction)
{
	const sf::Vector2i NORTH = sf::Vector2i(0, -1);
	const sf::Vector2i EAST = sf::Vector2i(1, 0);
	const sf::Vector2i SOUTH = sf::Vector2i(0, 1);
	const sf::Vector2i WEST = sf::Vector2i(-1, 0);
	const sf::Vector2i NORTHEAST = sf::Vector2i(1, -1);
	const sf::Vector2i NORTHWEST = sf::Vector2i(-1, -1);
	const sf::Vector2i SOUTHEAST = sf::Vector2i(1, 1);
	const sf::Vector2i SOUTHWEST = sf::Vector2i(-1, 1);
	Grid::CellList forcedNeighbors;

	if (direction == NORTH && grid.isTraversable(cell + NORTH))
	{
		if (!grid.isTraversable(cell + EAST) && grid.isTraversable(cell + NORTHEAST))
		{
			forcedNeighbors.push_back(cell + NORTHEAST);
		}
		if (!grid.isTraversable(cell + WEST) && grid.isTraversable(cell + NORTHWEST))
		{ 
			forcedNeighbors.push_back(cell + NORTHWEST); 
		}
	}
	else if (direction == EAST && grid.isTraversable(cell + EAST))
	{
		if (!grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + NORTHEAST))
		{
			forcedNeighbors.push_back(cell + NORTHEAST);
		}
		if (!grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + SOUTHEAST))
		{
			forcedNeighbors.push_back(cell + SOUTHEAST);
		}
	}
	else if (direction == SOUTH && grid.isTraversable(cell + SOUTH))
	{
		if (!grid.isTraversable(cell + EAST) && grid.isTraversable(cell + SOUTHEAST))
		{
			forcedNeighbors.push_back(cell + SOUTHEAST); 
		}
		if (!grid.isTraversable(cell + WEST) && grid.isTraversable(cell + SOUTHWEST))
		{ 
			forcedNeighbors.push_back(cell + SOUTHWEST); 
		}
	}
	else if (direction == WEST && grid.isTraversable(cell + WEST))
	{
		if (!grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + NORTHWEST))
		{ 
			forcedNeighbors.push_back(cell + NORTHWEST); 
		}
		if (!grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + SOUTHWEST))
		{
			forcedNeighbors.push_back(cell + SOUTHWEST); 
		}
	}
	else if (direction == NORTHEAST)
	{
		if (!grid.isTraversable(cell + WEST) && grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + NORTHWEST))
		{
			forcedNeighbors.push_back(cell + NORTHWEST);
		}
		if (!grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + EAST) && grid.isTraversable(cell + SOUTHEAST))
		{
			forcedNeighbors.push_back(cell + SOUTHEAST);
		}
	}
	else if (direction == NORTHWEST)
	{
		if (!grid.isTraversable(cell + EAST) && grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + NORTHEAST))
		{
			forcedNeighbors.push_back(cell + NORTHEAST);
		}
		if (!grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + WEST) && grid.isTraversable(cell + SOUTHWEST))
		{
			forcedNeighbors.push_back(cell + SOUTHWEST);
		}
	}
	else if (direction == SOUTHEAST)
	{
		if (!grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + EAST) && grid.isTraversable(cell + NORTHEAST))
		{
			forcedNeighbors.push_back(cell + NORTHEAST);
		}
		if (!grid.isTraversable(cell + WEST) && grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + SOUTHWEST))
		{
			forcedNeighbors.push_back(cell + SOUTHWEST);
		}
	}
	else    // southwest
	{
		if (!grid.isTraversable(cell + NORTH) && grid.isTraversable(cell + WEST) && grid.isTraversable(cell + NORTHWEST))
		{
			forcedNeighbors.push_back(cell + NORTHWEST);
		}
		if (!grid.isTraversable(cell + EAST) && grid.isTraversable(cell + SOUTH) && grid.isTraversable(cell + SOUTHEAST))
		{
			forcedNeighbors.push_back(cell + SOUTHEAST);
		}
	}

	return forcedNeighbors;
}

/*
* Return list of potential cells to be added to open list.
*
* @param grid: Grid instance.
* @param cell: 2D grid coordinate that serve as pivot of jump.
* @param direction: Direction from given cell and its parent. This is the propagation direction used for JPS' jump.
* @return: List of potential successor cells.
*/
Grid::CellList JPS::getSuccessors(Grid& grid, sf::Vector2i cell, sf::Vector2i direction)
{
	const sf::Vector2i NORTH = sf::Vector2i(0, -1);
	const sf::Vector2i EAST = sf::Vector2i(1, 0);
	const sf::Vector2i SOUTH = sf::Vector2i(0, 1);
	const sf::Vector2i WEST = sf::Vector2i(-1, 0);
	const sf::Vector2i NORTHEAST = sf::Vector2i(1, -1);
	const sf::Vector2i NORTHWEST = sf::Vector2i(-1, -1);
	const sf::Vector2i SOUTHEAST = sf::Vector2i(1, 1);
	const sf::Vector2i SOUTHWEST = sf::Vector2i(-1, 1);
	const sf::Vector2i NULL_VECTOR = sf::Vector2i(-1, -1);
	Grid::CellList successors;
	Grid::CellList neighbors;

	// consider all neighbors at start cell
	if (cell == grid.getStartCoordinate())
	{
		bool northIsTraversable = grid.isTraversable(cell + NORTH);
		bool eastIsTraversable = grid.isTraversable(cell + EAST);
		bool southIsTraversable = grid.isTraversable(cell + SOUTH);
		bool westIsTraversable = grid.isTraversable(cell + WEST);

		if (northIsTraversable) { neighbors.push_back(cell + NORTH); }
		if (eastIsTraversable) { neighbors.push_back(cell + EAST); }
		if (southIsTraversable) { neighbors.push_back(cell + SOUTH); }
		if (westIsTraversable) { neighbors.push_back(cell + WEST); }

		if (northIsTraversable || eastIsTraversable) { neighbors.push_back(cell + NORTHEAST); }
		if (northIsTraversable || westIsTraversable) { neighbors.push_back(cell + NORTHWEST); }
		if (southIsTraversable || eastIsTraversable) { neighbors.push_back(cell + SOUTHEAST); }
		if (southIsTraversable || westIsTraversable) { neighbors.push_back(cell + SOUTHWEST); }
	}

	// consider natural/forced neighbor at non-start cell
	else
	{
		// consider cell immediately in direction after coord
		neighbors.push_back(cell + direction);

		// consider additional neighbours for diagonal movement
		if (direction.x != 0 && direction.y != 0)
		{
			neighbors.push_back(cell + sf::Vector2i(direction.x, 0));
			neighbors.push_back(cell + sf::Vector2i(0, direction.y));
		}

		// get forced neighbours
		Grid::CellList forcedNeighbors = getForcedNeighbors(grid, cell, direction);
		neighbors.insert(neighbors.end(),
			std::make_move_iterator(forcedNeighbors.begin()),
			std::make_move_iterator(forcedNeighbors.end()));
	}

	// add significant neighbours only
	for (sf::Vector2i neighbor : neighbors)
	{
		sf::Vector2i jumpResult = jump(grid, cell, neighbor - cell);
		if (jumpResult != NULL_VECTOR) { successors.push_back(jumpResult); }
	}

	return successors;
}

/*
* Return a significant cell by recursive jumps. Return a NULL_VECTOR if no significant cells found.
*
* @param grid: Grid instance.
* @param cell: 2D grid coordinate that serve as pivot of jump.
* @param direction: Propagation direction of jump. Note that components x and y have unit lengths.
* @return: Significant cell to be considered in open list.
*/
sf::Vector2i JPS::jump(Grid& grid, sf::Vector2i cell, sf::Vector2i direction)
{
	const sf::Vector2i NULL_VECTOR = sf::Vector2i(-1, -1);
	sf::Vector2i newCell = cell + direction;

	// nothing to traverse
	if (!grid.isTraversable(newCell)) { return NULL_VECTOR; }

	// terminal cell is significant
	if (newCell == grid.getTerminalCoordinate()) { return newCell; }

	// current cell is significant due to forced neighbours
	if (getForcedNeighbors(grid, newCell, direction).size() != 0) { return newCell; }

	// diagonal movement require additional vertical/horizontal recursion
	if (direction.x != 0 && direction.y != 0)
	{
		// horizontal jump
		if (jump(grid, newCell, sf::Vector2i(direction.x, 0)) != NULL_VECTOR) { return newCell; }

		// vertical jump
		if (jump(grid, newCell, sf::Vector2i(0, direction.y)) != NULL_VECTOR) { return newCell; }

		// continue diagonal propagation
		return (grid.isTraversable(newCell + sf::Vector2i(direction.x, 0)) || grid.isTraversable(newCell + sf::Vector2i(0, direction.y)))
			? jump(grid, newCell, direction)
			: NULL_VECTOR;
	}

	// continue horizontal/vertical propagation
	return jump(grid, newCell, direction);
}

/**********************************************************************************************************************
**                                                      Getter                                                       **
**********************************************************************************************************************/

/*
* Construct path from terminal to starting cell. Note that cells within jump points are linearly interpolated.
*
* @param grid: Grid instance.
* @param cameFrom: 2D vector recording the origin of each cell.
* @param cutCorner: If true, no diagonal movement allowed beside corner.
* @return: List of coordinates going from terminal to starting cell.
*/
Grid::CellList JPS::getPath(Grid& grid, std::vector<Grid::CellList> cameFrom, bool cutCorner)
{
	const sf::Vector2i NULL_VECTOR = sf::Vector2i(-1, -1);
	Grid::CellList path = {grid.getTerminalCoordinate()};
	sf::Vector2i curr = grid.getTerminalCoordinate();

	// while current cell still has originator
	while (cameFrom[curr.x][curr.y] != NULL_VECTOR)
	{
		sf::Vector2i parent = cameFrom[curr.x][curr.y];
		sf::Vector2i direction = Distance::unitize(parent - curr);

		// do not cut corners with diagonal movement
		if (!cutCorner && direction.x != 0 && direction.y != 0)
		{
			if (!grid.isTraversable(curr + sf::Vector2i(direction.x, 0)))
			{ 
				path.push_back(curr + sf::Vector2i(0, direction.y));
			}
			else if (!grid.isTraversable(curr + sf::Vector2i(0, direction.y)))
			{
				path.push_back(curr + sf::Vector2i(direction.x, 0));
			}
		}
		sf::Vector2i interpCell = curr + direction;
		path.push_back(interpCell);

		while (interpCell != parent)
		{
			// do not cut corners with diagonal movement
			if (!cutCorner && direction.x != 0 && direction.y != 0)
			{
				if (!grid.isTraversable(interpCell + sf::Vector2i(direction.x, 0)))
				{ 
					path.push_back(interpCell + sf::Vector2i(0, direction.y));
				}
				else if (!grid.isTraversable(interpCell + sf::Vector2i(0, direction.y))) 
				{ 
					path.push_back(interpCell + sf::Vector2i(direction.x, 0));
				}
			}
			interpCell += direction;
			path.push_back(interpCell);
		}

		curr = parent;
	}

	return path;
}

/*
* Get potential cells to add to open list from given cell. This is mainly for inheritance.
*
* @param cell: 2D grid coordinate.
* @param parent: 2D grid coordinate from which the given cell came.
* @param cutCorner: If true, allows diagonal movement against wall. Default is false.
* @return: List of potential open list cells.
*/
Grid::CellList JPS::getPotentialCells(Grid& grid, sf::Vector2i cell, sf::Vector2i parent, bool cutCorner)
{
	return getSuccessors(grid, cell, Distance::unitize(cell - parent));
}
