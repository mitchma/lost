#include "../header/Distance.hpp"

/**********************************************************************************************************************
**                                                       Math                                                        **
**********************************************************************************************************************/
namespace Distance
{
	/*
	* Return Chebyshev distance between two coordinates.
	*
	* @param coord1: 2D coordinate.
	* @param coord2: 2D coordinate.
	* @return: Chebyshev distance. max(x, y).
	*/
	float chebyshevDistance(sf::Vector2i coord1, sf::Vector2i coord2)
	{
		float x = static_cast<float> (abs(coord1.x - coord2.x));
		float y = static_cast<float> (abs(coord1.y - coord2.y));
		return std::max(x, y);
	}

	/*
	* Return Euclidean distance (L2 norm) between two coordinates.
	*
	* @param coord1: 2D coordinate.
	* @param coord2: 2D coordinate.
	* @return: Euclidean distance. sqrt(dx^2 + dy^2).
	*/
	float euclideanDistance(sf::Vector2i coord1, sf::Vector2i coord2)
	{
		int x = coord1.x - coord2.x;
		int y = coord1.y - coord2.y;
		return static_cast<float> (sqrt(x*x + y * y));
	}

	/*
	* Return Manhattan distance (L1 norm) between two coordinates.
	*
	* @param coord1: 2D coordinate.
	* @param coord2: 2D coordinate.
	* @return: Manhattan distance. abs(dx) + abs(dy).
	*/
	float manhattanDistance(sf::Vector2i coord1, sf::Vector2i coord2)
	{
		int x = abs(coord1.x - coord2.x);
		int y = abs(coord1.y - coord2.y);
		return static_cast<float> (x + y);
	}

	/*
	* Return octile distance between two coordinates.
	*
	* @param coord1: 2D coordinate.
	* @param coord2: 2D coordinate.
	* @return: Octile distance. max(dx, dy) + (sqrt(2) - 1) * min(dx, dy).
	*/
	float octileDistance(sf::Vector2i coord1, sf::Vector2i coord2)
	{
		float x = static_cast<float> (abs(coord1.x - coord2.x));
		float y = static_cast<float> (abs(coord1.y - coord2.y));
		const float C = static_cast<float> (sqrt(2)) - 1;
		return x < y ? C * x + y : x + C * y;
	}

	/*
	* Convert given vector into vector with components of length 0 or 1. This is NOT normalization.
	*
	* @param vec: Vector.
	*/
	sf::Vector2i unitize(sf::Vector2i vec)
	{
		int xComponent = vec.x;
		int yComponent = vec.y;

		if (xComponent > 0) { xComponent = 1; }
		else if (xComponent < 0) { xComponent = -1; }

		if (yComponent > 0) { yComponent = 1; }
		else if (yComponent < 0) { yComponent = -1; }

		return sf::Vector2i(xComponent, yComponent);
	}
}