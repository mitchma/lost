
# Lost #

A pathfinding visualizer to help prototype some search algorithm implementations. This is mainly to test out some A* pathfinding variants.

## How do I get set up? ##

### Windows ###
* Clone the repository `git clone git@bitbucket.org:mitchma/lost.git`
* Open the `.sln` file with Visual Studio
* You may need to retarget the project to a different Windows SDK version (mine is 10.0.17134.0)
* Make sure that Debug or Release is set to x86
* Run!

## UI ##

### Cell Colors ###
* Green is the starting cell
* Red is the terminal cell
* Grey is untraversable wall
* Dark blue is an expanded cell
* Light blue is an open list cell

### Controls ###
* `W`, `A`, `S`, and `D` to move the grid around
* `LeftMouseButton` to drag the start and terminal cells
* `RightMouseButton` to place walls
* `LShift + RightMouseButton` to remove walls
* `Scroll wheel` to increase or decrease grid size
* `Space` to start the search

### Options ###
* Select between A* and JPS and different heuristics
* Enable or disable corner cutting
* Currently, animation cannot be stopped midway and must run until algorithm termination

![Jump point search animation](jps_animation.gif)
